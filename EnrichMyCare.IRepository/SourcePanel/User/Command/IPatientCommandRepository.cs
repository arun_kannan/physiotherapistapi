﻿using EnrichMyCare.Entity.User;
using EnrichMyCare.IRepository.Common;

namespace EnrichMyCare.IRepository.SourcePanel.User.Command
{
  public interface IPatientCommandRepository : IRepository<PatientDetails>
  {
  }
}