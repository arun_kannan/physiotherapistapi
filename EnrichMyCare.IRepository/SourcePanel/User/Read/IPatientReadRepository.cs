﻿using EnrichMyCare.DTO.SourcePanel.Result.User.Read;
using System.Collections.Generic;

namespace EnrichMyCare.IRepository.SourcePanel.User.Read
{
  public interface IPatientReadRepository
  {
    List<FindPatient> GetPatientDetails();
  }
}