﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;

namespace EnrichMyCare.IRepository.SourcePanel.Program.Command
{
  public interface ICreateExerciseCommandRepository
  {
    bool CreateExerciseProgram(CreateProgramParameter parameter);
  }
}