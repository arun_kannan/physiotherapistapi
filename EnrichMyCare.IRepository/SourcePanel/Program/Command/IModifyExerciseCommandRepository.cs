﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;

namespace EnrichMyCare.IRepository.SourcePanel.Program.Command
{
  public interface IModifyExerciseCommandRepository
  {
    bool ModifyExerciseProgram(ModifyProgramParameter parameter);
  }
}