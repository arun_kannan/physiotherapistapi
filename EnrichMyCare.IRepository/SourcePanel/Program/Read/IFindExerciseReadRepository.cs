﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.Entity.Program;
using System.Collections.Generic;
using EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder;

namespace EnrichMyCare.IRepository.SourcePanel.Program.Read
{
  public interface IFindExerciseReadRepository
  {
    List<ExerciseProgramResult> GetExerciseProgram(ExerciseProgram parameter);
    List<ExerciseResult> GetExercise(Exercise parameter);

    List<RepetitionsResult> GetRepetitions(Repetitions parameter);

    List<ExerciseProgramImagesResult> GetExerciseProgramImages(ExerciseProgramImages parameter);
  }
}