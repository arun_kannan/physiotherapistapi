﻿using EnrichMyCare.Entity;
using System.Collections.Generic;

namespace EnrichMyCare.IRepository.Common
{
  public interface IRepository<T> where T : BaseEntity
  {
    IEnumerable<T> GetAll();

    void Insert(T entity);

    void Update(T entity);

    void Delete(T entity);

    void Remove(T entity);

    void SaveChanges();
  }
}