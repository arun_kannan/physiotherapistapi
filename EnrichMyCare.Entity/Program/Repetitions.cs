﻿using System;
using EnrichMyCare.Entity.User;
using Newtonsoft.Json;

namespace EnrichMyCare.Entity.Program
{
  public class Repetitions : BaseEntity
  {
    public int RepetitionId { get; set; }
    public int ExerciseId { get; set; }
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public int Reps { get; set; }
    public int PerWeek { get; set; }
    public int TimePeriod { get; set; }

    //Parent
    [JsonIgnore]
    public virtual PatientDetails Patient { get; set; }

    [JsonIgnore]
    public virtual ExerciseProgram ExerciseProgram { get; set; }

    [JsonIgnore]
    public virtual Exercise Exercise { get; set; }
  }
}