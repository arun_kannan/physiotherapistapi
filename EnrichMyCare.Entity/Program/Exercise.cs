﻿using System;
using System.Collections.Generic;
using System.Text;
using EnrichMyCare.Entity.User;
using Newtonsoft.Json;

namespace EnrichMyCare.Entity.Program
{
 public  class Exercise
  {
    public int ExerciseId { get; set; }
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public string ExerciseName { get; set; }
    public DateTime ReviewDate { get; set; }
    public string ExerciseActivity { get; set; }

    //Parent
    [JsonIgnore]
    public virtual PatientDetails Patient { get; set; }

    [JsonIgnore]
    public virtual ExerciseProgram ExerciseProgram { get; set; }
    //Child
    [JsonIgnore]
    public ICollection<Repetitions> RepetitionsList { get; set; }
    [JsonIgnore]
    public ICollection<ExerciseProgramImages> ExerciseProgramImageList { get; set; }
  }
}
