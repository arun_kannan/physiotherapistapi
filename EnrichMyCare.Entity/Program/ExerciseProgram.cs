﻿using EnrichMyCare.Entity.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EnrichMyCare.Entity.Program
{
  public class ExerciseProgram : BaseEntity
  {
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public string NameExercise { get; set; }
    public DateTime ExerciseCreatedDate { get; set; }
    public string Goals { get; set; }
  

    //Parent
    [JsonIgnore]
    public virtual PatientDetails Patient { get; set; }


    //Child
    [JsonIgnore]
    public ICollection<Exercise> ExerciseList { get; set; }

    [JsonIgnore]
    public ICollection<Repetitions> RepetitionsList { get; set; }

    [JsonIgnore]
    public ICollection<ExerciseProgramImages> ExerciseProgramImageList { get; set; }
  }
}