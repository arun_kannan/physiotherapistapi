﻿using EnrichMyCare.Entity.User;
using Newtonsoft.Json;

namespace EnrichMyCare.Entity.Program
{
  public class ExerciseProgramImages : BaseEntity
  {
    public int ExerciseProgramImageId { get; set; }
    public int ExerciseId { get; set; }
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public string FileName { get; set; }
    public string FileUrl { get; set; }

    //Parent
    [JsonIgnore]
    public virtual PatientDetails Patient { get; set; }

    [JsonIgnore]
    public virtual ExerciseProgram ExerciseProgram { get; set; }
    [JsonIgnore]
    public virtual Exercise Exercise { get; set; }
  }
}