﻿using EnrichMyCare.Entity.Program;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace EnrichMyCare.Entity.User
{
  public class PatientDetails : BaseEntity
  {
    public int PatientNumber { get; set; }
    public string PatientName { get; set; }
    public string Diagnosis { get; set; }

    //Child
    [JsonIgnore]
    public ICollection<ExerciseProgram> ExerciseProgramList { get; set; }
    [JsonIgnore]
    public ICollection<Exercise> ExerciseList { get; set; }

    [JsonIgnore]
    public ICollection<ExerciseProgramImages> ExerciseProgramImagesList { get; set; }

    [JsonIgnore]
    public ICollection<Repetitions> RepetitionsList { get; set; }
  }
}