﻿namespace EnrichMyCare.ApplicationSettings
{
  public class FilePathConfiguration
  {
    public bool IsLog { get; set; }
    public bool IsShowTracer { get; set; }
    public bool IsProcess { get; set; }
    public string ErrorLogPath { get; set; }
    public string ServiceLogPath { get; set; }
    public string ProcessLogPath { get; set; }
  }
}