﻿namespace EnrichMyCare.ApplicationSettings
{
  public class AppIdentitySettings
  {
 
   
    public FilePathConfiguration FilePathConfiguration { get; set; }
    public ImageConfiguration ImageConfiguration { get; set; }
    public ApplicationFeatures ApplicationFeatures { get; set; }
  }
}