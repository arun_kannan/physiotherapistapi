﻿namespace EnrichMyCare.ApplicationSettings
{
  public class ImageConfiguration
  {
    public string NoImageUrl { get; set; }
    public string ProgramPath { get; set; }
    public string ProgramUrl { get; set; }
  }
}