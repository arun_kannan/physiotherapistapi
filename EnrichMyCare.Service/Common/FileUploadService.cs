﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.DTO;
using EnrichMyCare.DTO.SourcePanel.Common.Parameter;
using EnrichMyCare.DTO.SourcePanel.Common.Result;
using EnrichMyCare.IService.Common;
using EnrichMyCare.Utility;
using Microsoft.Extensions.Options;
using System;
using System.IO;

namespace EnrichMyCare.Service.Common
{
  public class FileUploadService : IFileUploadService
  {
    private static AppIdentitySettings _appIdentitySettings;

    public FileUploadService(IOptions<AppIdentitySettings> appIdentitySettingsAccessor)
    {
      _appIdentitySettings = appIdentitySettingsAccessor.Value;
    }

    public FileUploadResult SaveImageFile(FileUploadParameter parameter)
    {
      FileUploadResult serviceResult = new FileUploadResult() { ServiceStatus = new ServiceStatus() };
      try
      {
        if (!string.IsNullOrWhiteSpace(parameter.FileName) && !string.IsNullOrWhiteSpace(parameter.FileString) && parameter.FileString.Length > 0)
        {
          byte[] bytes = System.Convert.FromBase64String(parameter.FileString);
          CheckDirectory(parameter, out string filePath);
          if (!string.IsNullOrWhiteSpace(filePath))
          {
            var extension = Path.GetExtension(parameter.FileName);
            Guid guid = Guid.NewGuid();
            var generateFilename = string.Concat(guid.ToString().Replace("-", ""), extension);
            filePath = string.Concat(filePath, generateFilename);
            File.WriteAllBytes(filePath, bytes);
            FileHostedLocation(parameter, out string hostedLocation);
            serviceResult.FileDetail = new FileUpload
            {
              FileUrl = string.Concat(hostedLocation, generateFilename),
              FileName = generateFilename
            };
            serviceResult.ServiceStatus = new ServiceStatus() { Code = 100, Description = "Success" };
          }
          else
          {
            serviceResult.ServiceStatus = new ServiceStatus() { Code = 400, Description = "Missing Folder Name is Incorrect" };
          }
        }
        else
          serviceResult.ServiceStatus = new ServiceStatus() { Code = 300, Description = "Missing file name or file string" };
      }
      catch (Exception)
      {
        serviceResult.ServiceStatus = new ServiceStatus() { Code = 200, Description = "Server Exception" };
      }

      return serviceResult;
    }

    private static void CheckDirectory(FileUploadParameter parameter, out string filePath)
    {
      filePath = string.Empty;
      switch (parameter.FolderName)
      {
        case "Program":
          ImagesExtension.CreateDirectory(_appIdentitySettings.ImageConfiguration.ProgramPath);
          filePath = _appIdentitySettings.ImageConfiguration.ProgramPath;
          break;
      }
    }

    private static void FileHostedLocation(FileUploadParameter parameter, out string fileLocation)
    {
      fileLocation = string.Empty;
      switch (parameter.FolderName)
      {
        case "Program":
          fileLocation = _appIdentitySettings.ImageConfiguration.ProgramUrl;
          break;
      }
    }
  }
}