﻿using EnrichMyCare.IService.Common;
using EnrichMyCare.IService.SourcePanel.Program.Command;
using EnrichMyCare.IService.SourcePanel.Program.Read;
using EnrichMyCare.IService.SourcePanel.User.Command;
using EnrichMyCare.IService.SourcePanel.User.Read;
using EnrichMyCare.Service.Common;
using EnrichMyCare.Service.SourcePanel.Source.Program.Command;
using EnrichMyCare.Service.SourcePanel.Source.Program.Read;
using EnrichMyCare.Service.SourcePanel.Source.User.Command;
using EnrichMyCare.Service.SourcePanel.Source.User.Read;
using Microsoft.Extensions.DependencyInjection;

namespace EnrichMyCare.Service.SourcePanel.Dependency
{
  public static class ServiceSourcePanel
  {
    public static void Register(IServiceCollection services)
    {
      services.AddScoped(typeof(ICreateExerciseCommandService), typeof(CreateExerciseCommandService));
      services.AddScoped(typeof(IModifyExerciseCommandService), typeof(ModifyExerciseCommandService));
      services.AddScoped(typeof(IFindExerciseReadService), typeof(FindExerciseReadService));
      services.AddScoped(typeof(IPatientCommandService), typeof(PatientCommandService));
      services.AddScoped(typeof(IPatientReadService), typeof(PatientReadService));
      services.AddScoped(typeof(IFileUploadService), typeof(FileUploadService));
    }
  }
}