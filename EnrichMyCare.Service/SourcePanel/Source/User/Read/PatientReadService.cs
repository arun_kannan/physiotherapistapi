﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.DTO.SourcePanel.Enum;
using EnrichMyCare.DTO.SourcePanel.Result.User.Read;
using EnrichMyCare.IRepository.SourcePanel.User.Read;
using EnrichMyCare.IService.SourcePanel.User.Read;
using Microsoft.Extensions.Options;
using System;

namespace EnrichMyCare.Service.SourcePanel.Source.User.Read
{
  public class PatientReadService : IPatientReadService
  {
    private readonly IPatientReadRepository _patientReadRepository;
    private readonly AppIdentitySettings _appIdentitySettings;

    public PatientReadService(IOptions<AppIdentitySettings> appIdentitySettingsAccessor, IPatientReadRepository patientReadRepository)
    {
      _patientReadRepository = patientReadRepository;
      _appIdentitySettings = appIdentitySettingsAccessor.Value;
    }

    public FindPatientResult GetPatientDetails()
    {
      FindPatientResult result = new FindPatientResult();
      try
      {
        result.FindPatient = _patientReadRepository.GetPatientDetails();
        result.ServiceStatus = new DTO.ServiceStatus() { Code = (int)PatientUserEnum.PatientUserServiceResponse.Success, Description = "Success" };
      }
      catch (Exception e)
      {
        result.ServiceStatus = new DTO.ServiceStatus() { Code = (int)PatientUserEnum.PatientUserServiceResponse.InternalServerError, Description = "Server Error " };
      }
      return result;
    }
  }
}