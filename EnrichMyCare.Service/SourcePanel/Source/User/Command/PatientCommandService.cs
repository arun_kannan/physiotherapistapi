﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.DTO;
using EnrichMyCare.DTO.SourcePanel.Enum;
using EnrichMyCare.DTO.SourcePanel.Parameter.User.Command;
using EnrichMyCare.DTO.SourcePanel.Result.User.Command;
using EnrichMyCare.Entity.User;
using EnrichMyCare.IRepository.SourcePanel.User.Command;
using EnrichMyCare.IService.SourcePanel.User.Command;
using Microsoft.Extensions.Options;

namespace EnrichMyCare.Service.SourcePanel.Source.User.Command
{
  public class PatientCommandService : IPatientCommandService
  {
    private readonly AppIdentitySettings _appIdentitySettings;
    private readonly IPatientCommandRepository _patientCommandRepository;

    public PatientCommandService(IOptions<AppIdentitySettings> appIdentitySettingsAccessor, IPatientCommandRepository patientCommandRepository)
    {
      _patientCommandRepository = patientCommandRepository;
      _appIdentitySettings = appIdentitySettingsAccessor.Value;
    }

    public TOut DmlOperations<TIn, TOut>(TIn tin, CommonEnums.DMLOperation operationsMode)
      where TIn : class, new()
      where TOut : class, new()
    {
      TOut result = null;
      switch (operationsMode)
      {
        case CommonEnums.DMLOperation.Create:
          CreatePatientParameter createPatientParameter = tin as CreatePatientParameter;
          PatientDetails patientDetail = new PatientDetails();
          patientDetail.Diagnosis = createPatientParameter.Diagnosis;
          patientDetail.PatientName = createPatientParameter.PatientName;
          _patientCommandRepository.Insert(patientDetail);
          result = new CreatePatientResult() { ServiceStatus = new DTO.ServiceStatus() { Code = (int)PatientUserEnum.PatientUserServiceResponse.Success, Description = "Success" } } as TOut;
          break;

        case CommonEnums.DMLOperation.Remove:
          break;
      }
      return result;
    }
  }
}