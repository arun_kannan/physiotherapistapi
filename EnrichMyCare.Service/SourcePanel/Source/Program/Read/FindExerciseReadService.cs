﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.DTO.SourcePanel.Enum;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Read;
using EnrichMyCare.DTO.SourcePanel.Result.Program.Read;
using EnrichMyCare.Entity.Program;
using EnrichMyCare.IRepository.SourcePanel.Program.Read;
using EnrichMyCare.IService.SourcePanel.Program.Read;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder;

namespace EnrichMyCare.Service.SourcePanel.Source.Program.Read
{
  public class FindExerciseReadService : IFindExerciseReadService
  {
    private readonly AppIdentitySettings _appIdentitySettings;
    private readonly IFindExerciseReadRepository _findExerciseReadRepository;

    public FindExerciseReadService(IOptions<AppIdentitySettings> appIdentitySettingsAccessor, IFindExerciseReadRepository findExerciseReadRepository)
    {
      _findExerciseReadRepository = findExerciseReadRepository;
      _appIdentitySettings = appIdentitySettingsAccessor.Value;
    }

    public FindProgramResult GetExerciseProgram(FindProgramParameter parameter)
    {
      FindProgramResult result = new FindProgramResult() { ExerciseProgramList = new List<ExerciseProgramResult>() };
      try
      {
        result.ExerciseProgramList = _findExerciseReadRepository.GetExerciseProgram(new ExerciseProgram() { PatientNumber = parameter.PatientNumber });
        result.ExerciseProgramList?.ForEach(x =>
          {
            x.ExerciseResultList = _findExerciseReadRepository.GetExercise(new Exercise() { PatientNumber = parameter.PatientNumber, ExerciseProgramId = x.ExerciseProgramId });
            x.ExerciseResultList?.ForEach(exercise =>
            {
              exercise.RepetitionsList = _findExerciseReadRepository.GetRepetitions(new Repetitions() { PatientNumber = parameter.PatientNumber, ExerciseProgramId = x.ExerciseProgramId, ExerciseId = exercise.ExerciseId });
              x.ExerciseProgramImageList = _findExerciseReadRepository.GetExerciseProgramImages(new ExerciseProgramImages() { PatientNumber = parameter.PatientNumber, ExerciseProgramId = x.ExerciseProgramId, ExerciseId = exercise.ExerciseId });
              x.ExerciseProgramImageList?.ForEach(file =>
              {
                file.FileUrl = (!string.IsNullOrWhiteSpace(file.FileName)) ? string.Concat(_appIdentitySettings.ImageConfiguration.ProgramUrl, file.FileName) : string.Empty;
              });
            });

          
          });
        result.ServiceStatus = new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.Success, Description = "Success" };
      }
      catch (Exception e)
      {
        result.ServiceStatus = new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.InternalServerError, Description = "Server Error " };
      }
      return result;
    }
  }
}