﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.DTO.SourcePanel.Enum;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.DTO.SourcePanel.Result.Program.Command;
using EnrichMyCare.IRepository.SourcePanel.Program.Command;
using EnrichMyCare.IService.SourcePanel.Program.Command;
using Microsoft.Extensions.Options;
using System;

namespace EnrichMyCare.Service.SourcePanel.Source.Program.Command
{
  public class ModifyExerciseCommandService : IModifyExerciseCommandService
  {
    private readonly IModifyExerciseCommandRepository _modifyExerciseCommandRepository;
    private readonly AppIdentitySettings _appIdentitySettings;

    public ModifyExerciseCommandService(IOptions<AppIdentitySettings> appIdentitySettingsAccessor, IModifyExerciseCommandRepository modifyExerciseCommandRepository)
    {
      _modifyExerciseCommandRepository = modifyExerciseCommandRepository;
      _appIdentitySettings = appIdentitySettingsAccessor.Value;
    }

    public ModifyProgramResult ModifyExerciseProgram(ModifyProgramParameter parameter)
    {
      ModifyProgramResult result = new ModifyProgramResult();
      try
      {
        bool dbStatus = _modifyExerciseCommandRepository.ModifyExerciseProgram(parameter);
        result.ServiceStatus = (dbStatus) ? new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.Success, Description = "Success" } : new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.InternalServerError, Description = "DataBase Error" };
      }
      catch (Exception e)
      {
        result.ServiceStatus = new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.InternalServerError, Description = "Server Error " };
      }
      return result;
    }
  }
}