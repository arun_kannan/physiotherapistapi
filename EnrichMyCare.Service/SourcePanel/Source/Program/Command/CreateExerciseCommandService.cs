﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.DTO.SourcePanel.Enum;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.DTO.SourcePanel.Result.Program.Command;
using EnrichMyCare.IRepository.SourcePanel.Program.Command;
using EnrichMyCare.IService.SourcePanel.Program.Command;
using Microsoft.Extensions.Options;
using System;

namespace EnrichMyCare.Service.SourcePanel.Source.Program.Command
{
  public class CreateExerciseCommandService : ICreateExerciseCommandService
  {
    private readonly AppIdentitySettings _appIdentitySettings;
    private readonly ICreateExerciseCommandRepository _createExerciseCommandRepository;

    public CreateExerciseCommandService(IOptions<AppIdentitySettings> appIdentitySettingsAccessor, ICreateExerciseCommandRepository createExerciseCommandRepository)
    {
      _createExerciseCommandRepository = createExerciseCommandRepository;
      _appIdentitySettings = appIdentitySettingsAccessor.Value;
    }

    public CreateProgramResult CreateExerciseProgram(CreateProgramParameter parameter)
    {
      CreateProgramResult result = new CreateProgramResult();
      try
      {
        bool dbStatus = _createExerciseCommandRepository.CreateExerciseProgram(parameter);
        result.ServiceStatus = (dbStatus) ? new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.Success, Description = "Success" } : new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.InternalServerError, Description = "DataBase Error" };
      }
      catch (Exception e)
      {
        result.ServiceStatus = new DTO.ServiceStatus() { Code = (int)ProgramEnum.ProgramServiceResponse.InternalServerError, Description = "Server Error " };
      }
      return result;
    }
  }
}