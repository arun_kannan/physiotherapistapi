﻿using EnrichMyCare.Connector;
using EnrichMyCare.Entity;
using EnrichMyCare.IRepository.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EnrichMyCare.Repository.Common
{
  public class Repository<T> : IRepository<T> where T : BaseEntity
  {
    private readonly ApplicationContext _context;
    private readonly DbSet<T> _entities;
    private string _errorMessage = string.Empty;

    protected Repository(ApplicationContext context)
    {
      _context = context;
      _entities = context.Set<T>();
    }

    public void Delete(T entity)
    {
      if (entity == null) throw new ArgumentNullException(nameof(entity));
      _entities.Remove(entity);
      _context.SaveChanges();
    }

    public IEnumerable<T> GetAll()
    {
      return _entities.AsEnumerable();
    }

    public void Insert(T entity)
    {
      if (entity == null) throw new ArgumentNullException(nameof(entity));
      _entities.Add(entity);
      _context.SaveChanges();
    }

    public void Remove(T entity)
    {
      if (entity == null) throw new ArgumentNullException(nameof(entity));
      _entities.Remove(entity);
    }

    public void SaveChanges()
    {
      _context.SaveChanges();
    }

    public void Update(T entity)
    {
      if (entity == null) throw new ArgumentNullException(nameof(entity));
      _entities.Update(entity);
      _context.SaveChanges();
    }
  }
}