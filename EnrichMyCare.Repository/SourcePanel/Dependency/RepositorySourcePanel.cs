﻿using EnrichMyCare.IRepository.SourcePanel.Program.Command;
using EnrichMyCare.IRepository.SourcePanel.Program.Read;
using EnrichMyCare.IRepository.SourcePanel.User.Command;
using EnrichMyCare.IRepository.SourcePanel.User.Read;
using EnrichMyCare.Repository.SourcePanel.Program.Command;
using EnrichMyCare.Repository.SourcePanel.Program.Read;
using EnrichMyCare.Repository.SourcePanel.User.Read;
using Microsoft.Extensions.DependencyInjection;

namespace EnrichMyCare.Repository.SourcePanel.Dependency
{
  public static class RepositorySourcePanel
  {
    public static void Register(IServiceCollection services)
    {
      services.AddScoped(typeof(ICreateExerciseCommandRepository), typeof(CreateExerciseCommandRepository));
      services.AddScoped(typeof(IModifyExerciseCommandRepository), typeof(ModifyExerciseCommandRepository));
      services.AddScoped(typeof(IFindExerciseReadRepository), typeof(FindExerciseReadRepository));
      services.AddScoped(typeof(IPatientCommandRepository), typeof(PatientCommandRepository));
      services.AddScoped(typeof(IPatientReadRepository), typeof(PatientReadRepository));
    }
  }
}