﻿using EnrichMyCare.Connector;
using EnrichMyCare.DTO.SourcePanel.Result.User.Read;
using EnrichMyCare.IRepository.SourcePanel.User.Read;
using System.Collections.Generic;
using System.Linq;
using EnrichMyCare.Entity.User;

namespace EnrichMyCare.Repository.SourcePanel.User.Read
{
  public class PatientReadRepository : IPatientReadRepository
  {
    private readonly ApplicationContext _context;

    public PatientReadRepository(ApplicationContext context)
    {
      _context = context;
    }

    public List<FindPatient> GetPatientDetails()
    {
      return _context.Set<PatientDetails>()
        .Select(s => new FindPatient
        {
          PatientNumber = s.PatientNumber,
          PatientName = s.PatientName,
          Diagnosis = s.Diagnosis
        }).ToList();
    }
  }
}