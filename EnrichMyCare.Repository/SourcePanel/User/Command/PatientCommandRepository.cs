﻿using EnrichMyCare.Connector;
using EnrichMyCare.Entity.User;
using EnrichMyCare.Repository.Common;

namespace EnrichMyCare.IRepository.SourcePanel.User.Command
{
  public class PatientCommandRepository : Repository<PatientDetails>, IPatientCommandRepository
  {
    private readonly ApplicationContext _context;

    public PatientCommandRepository(ApplicationContext context) : base(context)
    {
      _context = context;
    }
  }
}