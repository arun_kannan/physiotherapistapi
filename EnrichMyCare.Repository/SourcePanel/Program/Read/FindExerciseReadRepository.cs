﻿using System.Collections.Generic;
using System.Linq;
using EnrichMyCare.Connector;
using EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder;
using EnrichMyCare.Entity.Program;
using EnrichMyCare.IRepository.SourcePanel.Program.Read;

namespace EnrichMyCare.Repository.SourcePanel.Program.Read
{
  public class FindExerciseReadRepository : IFindExerciseReadRepository
  {
    private readonly ApplicationContext _context;

    public FindExerciseReadRepository(ApplicationContext context)
    {
      _context = context;
    }

    public List<ExerciseResult> GetExercise(Exercise parameter)
    {
      return _context.Set<Exercise>().Where(x => x.PatientNumber == parameter.PatientNumber && x.ExerciseProgramId == parameter.ExerciseProgramId)
        .Select(s => new ExerciseResult
        {
          PatientNumber = s.PatientNumber,
          ExerciseProgramId = s.ExerciseProgramId,
          ExerciseActivity = s.ExerciseActivity,
          ExerciseId = s.ExerciseId,
          ReviewDate = s.ReviewDate,
          ExerciseName = s.ExerciseName,
        }).ToList();
    }

    public List<ExerciseProgramResult> GetExerciseProgram(ExerciseProgram parameter)
    {
      return _context.Set<ExerciseProgram>().Where(x => x.PatientNumber == parameter.PatientNumber)
        .Select(s => new ExerciseProgramResult
        {
          PatientNumber = s.PatientNumber,
          ExerciseProgramId = s.ExerciseProgramId,
          ExerciseCreatedDate = s.ExerciseCreatedDate,
          Goals = s.Goals,
          NameExercise = s.NameExercise,

        }).ToList();
    }

    public List<ExerciseProgramImagesResult> GetExerciseProgramImages(ExerciseProgramImages parameter)
    {
      return _context.Set<ExerciseProgramImages>()
        .Where(x => x.PatientNumber == parameter.PatientNumber && x.ExerciseProgramId == parameter.ExerciseProgramId && x.ExerciseId == parameter.ExerciseId)
        .Select(s => new ExerciseProgramImagesResult
        {
          PatientNumber = s.PatientNumber,
          ExerciseProgramId = s.ExerciseProgramId,
          FileUrl = s.FileUrl,
          FileName = s.FileName,
          ExerciseProgramImageId = s.ExerciseProgramImageId
        }).ToList();
    }

    public List<RepetitionsResult> GetRepetitions(Repetitions parameter)
    {
      return _context.Set<Repetitions>()
        .Where(x => x.PatientNumber == parameter.PatientNumber && x.ExerciseProgramId == parameter.ExerciseProgramId && x.ExerciseId == parameter.ExerciseId)
        .Select(s => new RepetitionsResult
        {
          PatientNumber = s.PatientNumber,
          ExerciseProgramId = s.ExerciseProgramId,
          ExerciseId = s.ExerciseId,
          Reps = s.Reps,
          PerWeek = s.PerWeek,
          TimePeriod = s.TimePeriod,
          RepetitionId = s.RepetitionId
        }).ToList();
    }
  }
}