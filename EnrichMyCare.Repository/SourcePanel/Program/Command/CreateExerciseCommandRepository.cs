﻿using EnrichMyCare.Connector;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.Entity.Program;
using EnrichMyCare.IRepository.SourcePanel.Program.Command;
using System;
using System.Linq;

namespace EnrichMyCare.Repository.SourcePanel.Program.Command
{
  public class CreateExerciseCommandRepository : ICreateExerciseCommandRepository
  {
    private readonly ApplicationContext _context;

    public CreateExerciseCommandRepository(ApplicationContext context)
    {
      _context = context;
    }

    public bool CreateExerciseProgram(CreateProgramParameter parameter)
    {
      bool result = false;
      using (var transaction = _context.Database.BeginTransaction())
      {
        try
        {
          var maxExerciseProgramId = _context.Set<ExerciseProgram>().Where(x => x.PatientNumber == parameter.PatientNumber).DefaultIfEmpty().Max(p => p == null ? 0 : p.ExerciseProgramId);
          ExerciseProgram exerciseProgram = new ExerciseProgram()
          {
            PatientNumber = parameter.PatientNumber,
            ExerciseProgramId = maxExerciseProgramId + 1,
            ExerciseCreatedDate = parameter.ExerciseProgram.ExerciseCreatedDate,
            Goals = parameter.ExerciseProgram.Goals,
            NameExercise = parameter.ExerciseProgram.NameExercise,

          };
          _context.Set<ExerciseProgram>().Add(exerciseProgram);
          _context.SaveChanges();
          for (var i = 0; i < parameter.ExerciseList.Count; i++)
          {
            Exercise exercise = new Exercise
            {
              PatientNumber = parameter.PatientNumber,
              ExerciseProgramId = maxExerciseProgramId + 1,
              ExerciseId = i + 1,
              ExerciseName = parameter.ExerciseList[i].ExerciseName,
              ExerciseActivity = parameter.ExerciseList[i].ExerciseActivity,
              ReviewDate = parameter.ExerciseList[i].ReviewDate
            };
            _context.Set<Exercise>().Add(exercise);
            _context.SaveChanges();

            for (var j = 0; j < parameter.ExerciseList[i].RepetitionList.Count; j++)
            {
              Repetitions repetitions = new Repetitions()
              {
                PatientNumber = parameter.PatientNumber,
                ExerciseProgramId = maxExerciseProgramId + 1,
                ExerciseId = i + 1,
                RepetitionId = j + 1,
                Reps = parameter.ExerciseList[i].RepetitionList[j].Reps,
                PerWeek = parameter.ExerciseList[i].RepetitionList[j].PerWeek,
                TimePeriod = parameter.ExerciseList[i].RepetitionList[j].TimePeriod
              };
              _context.Set<Repetitions>().Add(repetitions);
              _context.SaveChanges();
            }
            for (var k = 0; k < parameter.ExerciseList[i].ExerciseProgramImagesList.Count; k++)
            {
              ExerciseProgramImages exerciseProgramImages = new ExerciseProgramImages()
              {
                PatientNumber = parameter.PatientNumber,
                ExerciseProgramId = maxExerciseProgramId + 1,
                ExerciseId = i + 1,
                ExerciseProgramImageId = k + 1,
                FileName = parameter.ExerciseList[i].ExerciseProgramImagesList[k].FileName,
                FileUrl = parameter.ExerciseList[i].ExerciseProgramImagesList[k].FileUrl,
              };
              _context.Set<ExerciseProgramImages>().Add(exerciseProgramImages);
              _context.SaveChanges();
            }
          }
          transaction.Commit();
          result = true;
        }
        catch (Exception)
        {
          transaction.Rollback();
          throw;
        }
      }

      return result;
    }
  }
}