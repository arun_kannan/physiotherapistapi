﻿using EnrichMyCare.Connector;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.Entity.Program;
using EnrichMyCare.IRepository.SourcePanel.Program.Command;
using System;
using System.Linq;

namespace EnrichMyCare.Repository.SourcePanel.Program.Command
{
  public class ModifyExerciseCommandRepository : IModifyExerciseCommandRepository
  {
    private readonly ApplicationContext _context;

    public ModifyExerciseCommandRepository(ApplicationContext context)
    {
      _context = context;
    }

    public bool ModifyExerciseProgram(ModifyProgramParameter parameter)
    {
      bool result = false;
      var exerciseProgram = _context.Set<ExerciseProgram>().SingleOrDefault(x => x.PatientNumber == parameter.PatientNumber && x.ExerciseProgramId == parameter.ExerciseProgramId);
      if (exerciseProgram != null)
        RemoveProgram(parameter, exerciseProgram);
      result = CreateProgram(parameter);

      return result;
    }

    private void RemoveProgram(ModifyProgramParameter parameter, ExerciseProgram exerciseProgram)
    {
      using (var transaction = _context.Database.BeginTransaction())
      {
        try
        {


          var repetitionList = _context.Set<Repetitions>().Where(x =>
            x.PatientNumber == parameter.PatientNumber && x.ExerciseProgramId == parameter.ExerciseProgramId);
          _context.Set<Repetitions>().RemoveRange(repetitionList);
          _context.SaveChanges();

          var exerciseList = _context.Set<Exercise>().Where(x =>
            x.PatientNumber == parameter.PatientNumber && x.ExerciseProgramId == parameter.ExerciseProgramId);
          _context.Set<Exercise>().RemoveRange(exerciseList);
          _context.SaveChanges();


          var exerciseProgramImagesList = _context.Set<ExerciseProgramImages>().Where(x =>
            x.PatientNumber == parameter.PatientNumber && x.ExerciseProgramId == parameter.ExerciseProgramId);
          _context.Set<ExerciseProgramImages>().RemoveRange(exerciseProgramImagesList);
          _context.SaveChanges();

          _context.Set<ExerciseProgram>().Remove(exerciseProgram);
          _context.SaveChanges();
          transaction.Commit();
        }
        catch (Exception ex)
        {
          transaction.Rollback();
          throw;
        }
      }
    }

    private bool CreateProgram(ModifyProgramParameter parameter)
    {
      bool result;
      using (var transaction = _context.Database.BeginTransaction())
      {
        ;
        try
        {
          ExerciseProgram exerciseProgram = new ExerciseProgram()
          {
            PatientNumber = parameter.PatientNumber,
            ExerciseProgramId = parameter.ExerciseProgramId,
            ExerciseCreatedDate = parameter.ExerciseProgram.ExerciseCreatedDate,
            Goals = parameter.ExerciseProgram.Goals,
            NameExercise = parameter.ExerciseProgram.NameExercise,
          };
          _context.Set<ExerciseProgram>().Add(exerciseProgram);
          _context.SaveChanges();

          for (var i = 0; i < parameter.ExerciseList.Count; i++)
          {
            Exercise exercise = new Exercise
            {
              PatientNumber = parameter.PatientNumber,
              ExerciseProgramId = parameter.ExerciseProgramId,
              ExerciseId = i + 1,
              ExerciseName = parameter.ExerciseList[i].ExerciseName,
              ExerciseActivity = parameter.ExerciseList[i].ExerciseActivity,
              ReviewDate = parameter.ExerciseList[i].ReviewDate
            };
            _context.Set<Exercise>().Add(exercise);
            _context.SaveChanges();

            for (var j = 0; j < parameter.ExerciseList[i].RepetitionList.Count; j++)
            {
              Repetitions repetitions = new Repetitions()
              {
                PatientNumber = parameter.PatientNumber,
                ExerciseProgramId = parameter.ExerciseProgramId,
                ExerciseId = i + 1,
                RepetitionId = j + 1,
                Reps = parameter.ExerciseList[i].RepetitionList[j].Reps,
                PerWeek = parameter.ExerciseList[i].RepetitionList[j].PerWeek,
                TimePeriod = parameter.ExerciseList[i].RepetitionList[j].TimePeriod
              };
              _context.Set<Repetitions>().Add(repetitions);
              _context.SaveChanges();
            }
            for (var k = 0; k < parameter.ExerciseList[i].ExerciseProgramImagesList.Count; k++)
            {
              ExerciseProgramImages exerciseProgramImages = new ExerciseProgramImages()
              {
                PatientNumber = parameter.PatientNumber,
                ExerciseProgramId = parameter.ExerciseProgramId,
                ExerciseId = i + 1,
                ExerciseProgramImageId = k + 1,
                FileName = parameter.ExerciseList[i].ExerciseProgramImagesList[k].FileName,
                FileUrl = parameter.ExerciseList[i].ExerciseProgramImagesList[k].FileUrl,
              };
              _context.Set<ExerciseProgramImages>().Add(exerciseProgramImages);
              _context.SaveChanges();
            }
          }



          transaction.Commit();
          result = true;
        }
        catch (Exception)
        {
          transaction.Rollback();
          throw;
        }
      }

      return result;
    }
  }
}