﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.Connector;
using EnrichMyCare.DTO.SourcePanel.Common;
using EnrichMyCare.IRepository.Common;
using EnrichMyCare.Repository.Common;
using EnrichMyCare.Repository.SourcePanel.Dependency;
using EnrichMyCare.Service.SourcePanel.Dependency;
using EnrichMyCare.Web.Filters;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace EnrichMyCare.Web
{
  /// <summary>
  ///  Load Page
  /// </summary>
  public class Startup
  {
    private IConfiguration Configuration { get; }

    /// <summary>
    /// constructor
    /// </summary>
    /// <param name="configuration"></param>
    public Startup(IConfiguration configuration)
    {
      this.Configuration = configuration;
    }

    /// <summary>
    /// This method gets called by the runtime. Use this method to add services to the container.
    /// </summary>
    /// <param name="services"></param>
    public void ConfigureServices(IServiceCollection services)
    {
      var identitySettingsSection = this.Configuration.GetSection("AppIdentitySettings");
      services.Configure<AppIdentitySettings>(identitySettingsSection);

      //https://inneka.com/programming/angular/i-get-http-failure-response-for-unknown-url-0-unknown-error-instead-of-actual-error-message-in-angular/
      services.AddCors();
      services.AddMvc(setup =>
      {
        //   fv.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
      }).AddFluentValidation();
      services.AddMvc();
      //services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
      //{
      //  builder.WithOrigins("Access-Control-Allow-Origin", "*")
      //               .AllowAnyMethod()
      //               .AllowAnyHeader();
      //}));

      services.AddDbContext<ApplicationContext>(
          options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
          );

      services.AddScoped<ApiExceptionFilter>();
      services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

      //Repository
      RepositoryRegistration(services);

      //Service
      BusinessServiceRegistration(services);

      //Domain
      DomainValidationRegistration(services);

      services.Configure<IISOptions>(options =>
      {
        options.ForwardClientCertificate = false;
      });

      services.AddSwaggerGen(options =>
            {
              options.SwaggerDoc("v1", new Info { Title = "EnrichMyCare  API", Version = "v1" });

              // Get xml comments path
              var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
              var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

              // Set xml path
              options.IncludeXmlComments(xmlPath);
            });
    }

    /// <summary>
    /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    /// </summary>
    /// <param name="app"></param>
    /// <param name="env"></param>
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseBrowserLink();
        app.UseDeveloperExceptionPage();
        app.UseDatabaseErrorPage();
      }
      else
      {
        app.UseExceptionHandler("/Home/Error");
      }

      //app.UseCors(builder => builder.WithOrigins("http://localhost:4200")
      //                      .AllowAnyMethod()
      //                      .AllowAnyHeader());
      //app.UseCors("MyPolicy");
      //  app.UseMiddleware(typeof(CorsMiddleware));
      app.UseCors(options =>
        options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
      app.UseMvc();
      app.UseSwagger();
      InitializeDatabase(app);
      app.UseStaticFiles();

      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "EnrichMyCare API V1");
      });

      app.UseMvc(routes =>
      {
        routes.MapRoute(
                  name: "default",
                  template: "{controller=Home}/{action=Index}/{id?}");
      });
    }

    private void InitializeDatabase(IApplicationBuilder app)
    {
      using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
      {
        scope.ServiceProvider.GetRequiredService<ApplicationContext>().Database.Migrate();
      }
    }

    private static void DomainValidationRegistration(IServiceCollection services)
    {
      DomainSourcePanel.Register(services);
    }

    private static void BusinessServiceRegistration(IServiceCollection services)
    {
      ServiceSourcePanel.Register(services);
    }

    private static void RepositoryRegistration(IServiceCollection services)
    {
      RepositorySourcePanel.Register(services);
    }
  }
}