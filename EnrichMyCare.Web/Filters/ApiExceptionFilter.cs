﻿using EnrichMyCare.ApplicationSettings;
using EnrichMyCare.DTO;
using EnrichMyCare.Utility;
using EnrichMyCare.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System;
using System.Linq;

namespace EnrichMyCare.Web.Filters
{
  /// <summary>
  /// Exception Filter
  /// </summary>
  public class ApiExceptionFilter : ExceptionFilterAttribute
  {
    private readonly AppIdentitySettings _appIdentitySettings;
    private ExceptionContext _context { get; set; }

    public ApiExceptionFilter(IOptions<AppIdentitySettings> appIdentitySettingsAccessor)
    {
      _appIdentitySettings = appIdentitySettingsAccessor.Value;
    }

    /// <summary>
    /// Exception Occured Execution
    /// </summary>
    /// <param name="context"></param>
    public override void OnException(ExceptionContext context)
    {
      string errorId = Guid.NewGuid().ToString();
      context.Exception.ToMessageAndCompleteStacktrace(_appIdentitySettings, errorId);
      _context = context;
      context.Result = new JsonResult(InternalResponse(Convert.ToString(context.HttpContext.Items.FirstOrDefault(x => Convert.ToString(x.Key) == "ClassName").Value), errorId));
    }

    private SingleResponse<object> InternalResponse(string strFullyQualifiedName, string errorReference)
    {
      SingleResponse<Object> dynamicInstance = new SingleResponse<object> { Model = GetInstance(strFullyQualifiedName) };
      return InternalServerResponse(dynamicInstance, errorReference);
    }

    private object GetInstance(string strFullyQualifiedName)
    {
      Type type = Type.GetType(strFullyQualifiedName);
      if (type != null)
        return Activator.CreateInstance(type);
      foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
      {
        if (!string.IsNullOrWhiteSpace(strFullyQualifiedName))
        {
          type = asm.GetType(strFullyQualifiedName);
          if (type != null)
            return Activator.CreateInstance(type);
        }
      }
      return null;
    }

    private SingleResponse<dynamic> InternalServerResponse(SingleResponse<dynamic> response, string errorReference)
    {
      if (response.Model != null)
        response.Model.ServiceStatus = new ServiceStatus()
        {
          Code = (int)CommonEnums.CommonServiceResponse.InternalServerException,
          Description = errorReference
        };
      response.DidError = true;
      response.ErrorMessage = _appIdentitySettings.ApplicationFeatures.IsShowServerError ? _context.Exception.ToMessageAndCompleteStacktrace() : "There was an internal error, please contact to technical support.";
      response.Message = "There was an internal error, please contact to technical support.(ErrorID)" + errorReference;
      return response;
    }
  }
}