﻿using EnrichMyCare.DTO.SourcePanel.Common.Parameter;
using EnrichMyCare.DTO.SourcePanel.Common.Result;
using EnrichMyCare.IService.Common;
using EnrichMyCare.Web.Filters;
using EnrichMyCare.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EnrichMyCare.Web.Controllers.SourcePanel
{
  [ServiceFilter(typeof(ApiExceptionFilter))]
  [Produces("application/json")]
  [Route("api/FileUpload")]
  public class FileUploadController : Controller
  {
    private readonly IFileUploadService _fileUploadPresenter;

    public FileUploadController(IFileUploadService fileUploadPresenter)
    {
      _fileUploadPresenter = fileUploadPresenter;
    }

    [HttpPost("SaveImageFile")]
    [ProducesResponseType(typeof(SingleResponse<FileUploadResult>), 200)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> SaveImageFile([FromBody] FileUploadParameter parameter)
    {
      HttpContext.Items.Add("ClassName", typeof(FileUploadResult).AssemblyQualifiedName);
      var response = new SingleResponse<FileUploadResult> { Model = _fileUploadPresenter.SaveImageFile(parameter) };
      return response.ToHttpResponse();
    }
  }
}