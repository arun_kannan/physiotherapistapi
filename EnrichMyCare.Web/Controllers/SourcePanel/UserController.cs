﻿using EnrichMyCare.DTO;
using EnrichMyCare.DTO.SourcePanel.Parameter.User.Command;
using EnrichMyCare.DTO.SourcePanel.Result.User.Command;
using EnrichMyCare.DTO.SourcePanel.Result.User.Read;
using EnrichMyCare.IService.SourcePanel.User.Command;
using EnrichMyCare.IService.SourcePanel.User.Read;
using EnrichMyCare.Web.Filters;
using EnrichMyCare.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EnrichMyCare.Web.Controllers.SourcePanel
{
  [ServiceFilter(typeof(ApiExceptionFilter))]
  [Produces("application/json")]
  [Route("api/User")]
  public class UserController : ControllerBase
  {
    private readonly IPatientCommandService _patientCommandService;
    private readonly IPatientReadService _patientReadService;

    public UserController(IPatientCommandService patientCommandService, IPatientReadService patientReadService)
    {
      _patientCommandService = patientCommandService;
      _patientReadService = patientReadService;
    }

    /// <summary>
    ///  Create Patient
    /// </summary>
    /// <param name="parameter"></param>
    /// <returns></returns>

    [HttpPost("CreatePatient")]
    [ProducesResponseType(typeof(SingleResponse<CreatePatientResult>), 200)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> CreatePatient([FromBody] CreatePatientParameter parameter)
    {
      HttpContext.Items.Add("ClassName", typeof(CreatePatientResult).AssemblyQualifiedName);
      var response = new SingleResponse<CreatePatientResult>
      {
        Model = _patientCommandService.DmlOperations<CreatePatientParameter, CreatePatientResult>(parameter,
          CommonEnums.DMLOperation.Create)
      };
      return response.ToHttpResponse();
    }

    /// <summary>
    /// Get All Patient
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetPatientDetails")]
    [ProducesResponseType(typeof(SingleResponse<FindPatientResult>), 200)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> GetPatientDetails()
    {
      HttpContext.Items.Add("ClassName", typeof(FindPatientResult).AssemblyQualifiedName);
      var response = new SingleResponse<FindPatientResult>
      {
        Model = _patientReadService.GetPatientDetails()
      };
      return response.ToHttpResponse();
    }
  }
}