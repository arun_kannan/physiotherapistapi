﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Read;
using EnrichMyCare.DTO.SourcePanel.Result.Program.Command;
using EnrichMyCare.DTO.SourcePanel.Result.Program.Read;
using EnrichMyCare.IService.SourcePanel.Program.Command;
using EnrichMyCare.IService.SourcePanel.Program.Read;
using EnrichMyCare.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using EnrichMyCare.Web.Filters;

namespace EnrichMyCare.Web.Controllers.SourcePanel
{
  [ServiceFilter(typeof(ApiExceptionFilter))]
  [Produces("application/json")]
  [Route("api/ExerciseProgram")]
  public class ExerciseProgramController : ControllerBase
  {
    private readonly ICreateExerciseCommandService _createExerciseCommandService;
    private readonly IModifyExerciseCommandService _modifyExerciseCommandService;
    private readonly IFindExerciseReadService _findExerciseReadService;

    public ExerciseProgramController(ICreateExerciseCommandService createExerciseCommandService, IModifyExerciseCommandService modifyExerciseCommandService, IFindExerciseReadService findExerciseReadService)
    {
      _createExerciseCommandService = createExerciseCommandService;
      _modifyExerciseCommandService = modifyExerciseCommandService;
      _findExerciseReadService = findExerciseReadService;
    }

    [HttpPost("CreateExerciseProgram")]
    [ProducesResponseType(typeof(SingleResponse<CreateProgramResult>), 200)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> CreateExerciseProgram([FromBody] CreateProgramParameter parameter)
    {
      HttpContext.Items.Add("ClassName", typeof(CreateProgramResult).AssemblyQualifiedName);
      var response = new SingleResponse<CreateProgramResult>
      {
        Model = _createExerciseCommandService.CreateExerciseProgram(parameter)
      };
      return response.ToHttpResponse();
    }

    [HttpPost("ModifyExerciseProgram")]
    [ProducesResponseType(typeof(SingleResponse<ModifyProgramResult>), 200)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> ModifyExerciseProgram([FromBody] ModifyProgramParameter parameter)
    {
      HttpContext.Items.Add("ClassName", typeof(ModifyProgramResult).AssemblyQualifiedName);
      var response = new SingleResponse<ModifyProgramResult>
      {
        Model = _modifyExerciseCommandService.ModifyExerciseProgram(parameter)
      };
      return response.ToHttpResponse();
    }

    [HttpPost("GetExerciseProgram")]
    [ProducesResponseType(typeof(SingleResponse<FindProgramResult>), 200)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> GetExerciseProgram([FromBody] FindProgramParameter parameter)
    {
      HttpContext.Items.Add("ClassName", typeof(FindProgramResult).AssemblyQualifiedName);
      var response = new SingleResponse<FindProgramResult>
      {
        Model = _findExerciseReadService.GetExerciseProgram(parameter)
      };
      return response.ToHttpResponse();
    }
  }
}