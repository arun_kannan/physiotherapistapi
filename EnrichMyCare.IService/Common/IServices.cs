﻿using EnrichMyCare.DTO;

namespace EnrichMyCare.IService.Common
{
  public interface IServices
  {
    TOut DmlOperations<TIn, TOut>(TIn tin, CommonEnums.DMLOperation operationsMode)
      where TIn : class, new()
      where TOut : class, new();
  }
}