﻿using EnrichMyCare.DTO.SourcePanel.Common.Parameter;
using EnrichMyCare.DTO.SourcePanel.Common.Result;

namespace EnrichMyCare.IService.Common
{
  public interface IFileUploadService
  {
    FileUploadResult SaveImageFile(FileUploadParameter parameter);
  }
}