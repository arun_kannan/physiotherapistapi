﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Read;
using EnrichMyCare.DTO.SourcePanel.Result.Program.Read;

namespace EnrichMyCare.IService.SourcePanel.Program.Read
{
  public interface IFindExerciseReadService
  {
    FindProgramResult GetExerciseProgram(FindProgramParameter parameter);
  }
}