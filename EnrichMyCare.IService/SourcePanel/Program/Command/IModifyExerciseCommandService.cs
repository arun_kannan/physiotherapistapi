﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.DTO.SourcePanel.Result.Program.Command;

namespace EnrichMyCare.IService.SourcePanel.Program.Command
{
  public interface IModifyExerciseCommandService
  {
    ModifyProgramResult ModifyExerciseProgram(ModifyProgramParameter parameter);
  }
}