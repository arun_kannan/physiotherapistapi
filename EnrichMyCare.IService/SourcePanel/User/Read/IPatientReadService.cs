﻿using EnrichMyCare.DTO.SourcePanel.Result.User.Read;

namespace EnrichMyCare.IService.SourcePanel.User.Read
{
  public interface IPatientReadService
  {
    FindPatientResult GetPatientDetails();
  }
}