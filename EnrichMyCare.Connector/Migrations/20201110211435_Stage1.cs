﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EnrichMyCare.Connector.Migrations
{
    public partial class Stage1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PatientDetails",
                columns: table => new
                {
                    PatientNumber = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PatientName = table.Column<string>(maxLength: 80, nullable: false),
                    Diagnosis = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientDetails", x => x.PatientNumber);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseProgram",
                columns: table => new
                {
                    ExerciseProgramId = table.Column<int>(nullable: false),
                    PatientNumber = table.Column<int>(nullable: false),
                    NameExercise = table.Column<string>(maxLength: 200, nullable: false),
                    ExerciseCreatedDate = table.Column<DateTime>(nullable: false),
                    Goals = table.Column<string>(maxLength: 2000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseProgram", x => new { x.ExerciseProgramId, x.PatientNumber });
                    table.ForeignKey(
                        name: "FK_ExerciseProgram_PatientDetails_PatientNumber",
                        column: x => x.PatientNumber,
                        principalTable: "PatientDetails",
                        principalColumn: "PatientNumber",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Exercise",
                columns: table => new
                {
                    ExerciseId = table.Column<int>(nullable: false),
                    ExerciseProgramId = table.Column<int>(nullable: false),
                    PatientNumber = table.Column<int>(nullable: false),
                    ExerciseName = table.Column<string>(maxLength: 1000, nullable: false),
                    ReviewDate = table.Column<DateTime>(nullable: false),
                    ExerciseActivity = table.Column<string>(maxLength: 7000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exercise", x => new { x.ExerciseId, x.ExerciseProgramId, x.PatientNumber });
                    table.ForeignKey(
                        name: "FK_Exercise_PatientDetails_PatientNumber",
                        column: x => x.PatientNumber,
                        principalTable: "PatientDetails",
                        principalColumn: "PatientNumber",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Exercise_ExerciseProgram_ExerciseProgramId_PatientNumber",
                        columns: x => new { x.ExerciseProgramId, x.PatientNumber },
                        principalTable: "ExerciseProgram",
                        principalColumns: new[] { "ExerciseProgramId", "PatientNumber" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseProgramImages",
                columns: table => new
                {
                    ExerciseProgramImageId = table.Column<int>(nullable: false),
                    ExerciseId = table.Column<int>(nullable: false),
                    ExerciseProgramId = table.Column<int>(nullable: false),
                    PatientNumber = table.Column<int>(nullable: false),
                    FileName = table.Column<string>(maxLength: 200, nullable: false),
                    FileUrl = table.Column<string>(maxLength: 8000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseProgramImages", x => new { x.ExerciseProgramImageId, x.ExerciseId, x.ExerciseProgramId, x.PatientNumber });
                    table.ForeignKey(
                        name: "FK_ExerciseProgramImages_PatientDetails_PatientNumber",
                        column: x => x.PatientNumber,
                        principalTable: "PatientDetails",
                        principalColumn: "PatientNumber",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ExerciseProgramImages_ExerciseProgram_ExerciseProgramId_PatientNumber",
                        columns: x => new { x.ExerciseProgramId, x.PatientNumber },
                        principalTable: "ExerciseProgram",
                        principalColumns: new[] { "ExerciseProgramId", "PatientNumber" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ExerciseProgramImages_Exercise_ExerciseId_ExerciseProgramId_PatientNumber",
                        columns: x => new { x.ExerciseId, x.ExerciseProgramId, x.PatientNumber },
                        principalTable: "Exercise",
                        principalColumns: new[] { "ExerciseId", "ExerciseProgramId", "PatientNumber" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Repetitions",
                columns: table => new
                {
                    RepetitionId = table.Column<int>(nullable: false),
                    ExerciseId = table.Column<int>(nullable: false),
                    ExerciseProgramId = table.Column<int>(nullable: false),
                    PatientNumber = table.Column<int>(nullable: false),
                    Reps = table.Column<int>(nullable: false),
                    PerWeek = table.Column<int>(nullable: false),
                    TimePeriod = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Repetitions", x => new { x.RepetitionId, x.ExerciseId, x.ExerciseProgramId, x.PatientNumber });
                    table.ForeignKey(
                        name: "FK_Repetitions_PatientDetails_PatientNumber",
                        column: x => x.PatientNumber,
                        principalTable: "PatientDetails",
                        principalColumn: "PatientNumber",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Repetitions_ExerciseProgram_ExerciseProgramId_PatientNumber",
                        columns: x => new { x.ExerciseProgramId, x.PatientNumber },
                        principalTable: "ExerciseProgram",
                        principalColumns: new[] { "ExerciseProgramId", "PatientNumber" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Repetitions_Exercise_ExerciseId_ExerciseProgramId_PatientNumber",
                        columns: x => new { x.ExerciseId, x.ExerciseProgramId, x.PatientNumber },
                        principalTable: "Exercise",
                        principalColumns: new[] { "ExerciseId", "ExerciseProgramId", "PatientNumber" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Exercise_PatientNumber",
                table: "Exercise",
                column: "PatientNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Exercise_ExerciseProgramId_PatientNumber",
                table: "Exercise",
                columns: new[] { "ExerciseProgramId", "PatientNumber" });

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseProgram_PatientNumber",
                table: "ExerciseProgram",
                column: "PatientNumber");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseProgramImages_PatientNumber",
                table: "ExerciseProgramImages",
                column: "PatientNumber");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseProgramImages_ExerciseProgramId_PatientNumber",
                table: "ExerciseProgramImages",
                columns: new[] { "ExerciseProgramId", "PatientNumber" });

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseProgramImages_ExerciseId_ExerciseProgramId_PatientNumber",
                table: "ExerciseProgramImages",
                columns: new[] { "ExerciseId", "ExerciseProgramId", "PatientNumber" });

            migrationBuilder.CreateIndex(
                name: "IX_Repetitions_PatientNumber",
                table: "Repetitions",
                column: "PatientNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Repetitions_ExerciseProgramId_PatientNumber",
                table: "Repetitions",
                columns: new[] { "ExerciseProgramId", "PatientNumber" });

            migrationBuilder.CreateIndex(
                name: "IX_Repetitions_ExerciseId_ExerciseProgramId_PatientNumber",
                table: "Repetitions",
                columns: new[] { "ExerciseId", "ExerciseProgramId", "PatientNumber" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExerciseProgramImages");

            migrationBuilder.DropTable(
                name: "Repetitions");

            migrationBuilder.DropTable(
                name: "Exercise");

            migrationBuilder.DropTable(
                name: "ExerciseProgram");

            migrationBuilder.DropTable(
                name: "PatientDetails");
        }
    }
}
