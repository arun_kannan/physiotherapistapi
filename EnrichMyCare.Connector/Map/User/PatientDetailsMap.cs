﻿using EnrichMyCare.Entity.User;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EnrichMyCare.Connector.Map.User
{
  public class PatientDetailsMap
  {
    public PatientDetailsMap(EntityTypeBuilder<PatientDetails> entityBuilder)
    {
      entityBuilder.HasKey(t => t.PatientNumber);
      entityBuilder.Property(t => t.PatientName).IsRequired().HasMaxLength(80);
      entityBuilder.Property(t => t.Diagnosis).IsRequired().HasMaxLength(200);
    }
  }
}