﻿using EnrichMyCare.Entity.Program;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EnrichMyCare.Connector.Map.Program
{
  public class ExerciseProgramImagesMap
  {
    public ExerciseProgramImagesMap(EntityTypeBuilder<ExerciseProgramImages> entityBuilder)
    {
      entityBuilder.HasKey(t => new { t.ExerciseProgramImageId, t.ExerciseId,t.ExerciseProgramId, t.PatientNumber });
      entityBuilder.Property(t => t.ExerciseId).IsRequired();
      entityBuilder.Property(t => t.ExerciseProgramId).IsRequired();
      entityBuilder.Property(t => t.PatientNumber).IsRequired();
      entityBuilder.Property(t => t.FileName).IsRequired().HasMaxLength(200);
      entityBuilder.Property(t => t.FileUrl).HasMaxLength(8000);
      entityBuilder.HasOne(t => t.ExerciseProgram).WithMany(a => a.ExerciseProgramImageList).HasForeignKey(z => new { z.ExerciseProgramId, z.PatientNumber });
      entityBuilder.HasOne(t => t.Patient).WithMany(a => a.ExerciseProgramImagesList).HasForeignKey(z => z.PatientNumber);
      entityBuilder.HasOne(t => t.Exercise).WithMany(a => a.ExerciseProgramImageList).HasForeignKey(z => new { z.ExerciseId, z.ExerciseProgramId, z.PatientNumber });
    }
  }
}