﻿using EnrichMyCare.Entity.Program;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EnrichMyCare.Connector.Map.Program
{
  public class RepetitionsMap
  {
    public RepetitionsMap(EntityTypeBuilder<Repetitions> entityBuilder)
    {
      entityBuilder.HasKey(t => new { t.RepetitionId, t.ExerciseId, t.ExerciseProgramId, t.PatientNumber });
      entityBuilder.Property(t => t.ExerciseId).IsRequired();
      entityBuilder.Property(t => t.ExerciseProgramId).IsRequired();
      entityBuilder.Property(t => t.PatientNumber).IsRequired();
      entityBuilder.Property(t => t.Reps).IsRequired();
      entityBuilder.Property(t => t.PerWeek).IsRequired();
      entityBuilder.Property(t => t.TimePeriod).IsRequired();
      entityBuilder.HasOne(t => t.ExerciseProgram).WithMany(a => a.RepetitionsList).HasForeignKey(z => new { z.ExerciseProgramId, z.PatientNumber });
      entityBuilder.HasOne(t => t.Patient).WithMany(a => a.RepetitionsList).HasForeignKey(z => z.PatientNumber);
      entityBuilder.HasOne(t => t.Exercise).WithMany(a => a.RepetitionsList).HasForeignKey(z => new { z.ExerciseId, z.ExerciseProgramId, z.PatientNumber });
    }
  }
} 