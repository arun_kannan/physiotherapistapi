﻿using System;
using System.Collections.Generic;
using System.Text;
using EnrichMyCare.Entity.Program;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EnrichMyCare.Connector.Map.Program
{
 public  class ExerciseMap
  {
    public ExerciseMap(EntityTypeBuilder<Exercise> entityBuilder)
    {
      entityBuilder.HasKey(t => new {t.ExerciseId ,t.ExerciseProgramId, t.PatientNumber });
      entityBuilder.Property(t => t.ExerciseProgramId).IsRequired();
      entityBuilder.Property(t => t.PatientNumber).IsRequired();
      entityBuilder.Property(t => t.ExerciseName).IsRequired().HasMaxLength(1000);
      entityBuilder.Property(t => t.ReviewDate).IsRequired();
      entityBuilder.Property(t => t.ExerciseActivity).IsRequired().HasMaxLength(7000);
      entityBuilder.HasOne(t => t.Patient).WithMany(a => a.ExerciseList).HasForeignKey(z => z.PatientNumber);
      entityBuilder.HasOne(t => t.ExerciseProgram).WithMany(a => a.ExerciseList).HasForeignKey(z => new { z.ExerciseProgramId, z.PatientNumber });
    }
  }
}
