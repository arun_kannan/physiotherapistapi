﻿using EnrichMyCare.Entity.Program;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EnrichMyCare.Connector.Map.Program
{
  public class ExerciseProgramMap
  {
    public ExerciseProgramMap(EntityTypeBuilder<ExerciseProgram> entityBuilder)
    {
      entityBuilder.HasKey(t => new { t.ExerciseProgramId, t.PatientNumber });
      entityBuilder.Property(t => t.PatientNumber).IsRequired();
      entityBuilder.Property(t => t.NameExercise).IsRequired().HasMaxLength(200);
      entityBuilder.Property(t => t.ExerciseCreatedDate).IsRequired();
      entityBuilder.Property(t => t.Goals).IsRequired().HasMaxLength(2000);
   entityBuilder.HasOne(t => t.Patient).WithMany(a => a.ExerciseProgramList).HasForeignKey(z => z.PatientNumber);
    }
  }
}