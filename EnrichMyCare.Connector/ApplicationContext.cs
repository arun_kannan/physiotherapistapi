﻿using EnrichMyCare.Connector.Map.Program;
using EnrichMyCare.Connector.Map.User;
using EnrichMyCare.Entity.Program;
using EnrichMyCare.Entity.User;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EnrichMyCare.Connector
{
  public class ApplicationContext : DbContext
  {
    public ApplicationContext()
    {
    }

    public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);
      new PatientDetailsMap(modelBuilder.Entity<PatientDetails>());
      new ExerciseProgramMap(modelBuilder.Entity<ExerciseProgram>());
      new RepetitionsMap(modelBuilder.Entity<Repetitions>());
      new ExerciseProgramImagesMap(modelBuilder.Entity<ExerciseProgramImages>());
      new ExerciseMap(modelBuilder.Entity<Exercise>());
      foreach (var property in modelBuilder.Model.GetEntityTypes()
        .SelectMany(t => t.GetProperties())
        .Where(p => p.ClrType == typeof(decimal)))
      {
        property.Relational().ColumnType = "decimal(9, 2)";
      }
    }
  }
}