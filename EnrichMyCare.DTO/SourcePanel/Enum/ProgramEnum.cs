﻿namespace EnrichMyCare.DTO.SourcePanel.Enum
{
  public class ProgramEnum
  {
    public enum ProgramServiceResponse
    {
      Success = 100,
      DescriptionExits = 1,
      InternalServerError = 500,
      ParameterNullReference = 300,
    }
  }
}