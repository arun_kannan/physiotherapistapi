﻿namespace EnrichMyCare.DTO.SourcePanel.Enum
{
  public class PatientUserEnum
  {
    public enum PatientUserServiceResponse
    {
      Success = 100,
      InternalServerError = 500,
      ParameterNullReference = 300,
    }
  }
}