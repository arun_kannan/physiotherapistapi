﻿using System;

namespace EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command
{
  public class ExerciseProgramParameter
  {
    public string NameExercise { get; set; }
    public DateTime ExerciseCreatedDate { get; set; }
    public string Goals { get; set; }
  
  }
}