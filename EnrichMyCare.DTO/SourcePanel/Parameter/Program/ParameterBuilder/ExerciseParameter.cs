﻿using System;
using System.Collections.Generic;
using System.Text;
using EnrichMyCare.Entity.Program.ParameterBuilder;

namespace EnrichMyCare.DTO.SourcePanel.Parameter.Program.ParameterBuilder
{
  public class ExerciseParameter
  {
    public string ExerciseName { get; set; }
    public DateTime ReviewDate { get; set; }
    public string ExerciseActivity { get; set; }

    public List<RepetitionsParameter> RepetitionList { get; set; }
    public List<ExerciseProgramImagesParameter> ExerciseProgramImagesList { get; set; }
  }
}
