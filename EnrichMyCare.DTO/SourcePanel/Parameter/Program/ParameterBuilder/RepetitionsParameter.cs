﻿namespace EnrichMyCare.DTO.SourcePanel.Parameter.Program.ParameterBuilder
{
  public class RepetitionsParameter
  {
    public int Reps { get; set; }
    public int PerWeek { get; set; }
    public int TimePeriod { get; set; }
  }
}