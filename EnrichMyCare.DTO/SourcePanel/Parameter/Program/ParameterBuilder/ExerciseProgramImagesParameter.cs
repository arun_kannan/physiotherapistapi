﻿namespace EnrichMyCare.Entity.Program.ParameterBuilder
{
  public class ExerciseProgramImagesParameter
  {
    public string FileName { get; set; }
    public string FileUrl { get; set; }
  }
}