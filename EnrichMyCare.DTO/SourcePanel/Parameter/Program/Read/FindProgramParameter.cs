﻿using FluentValidation;

namespace EnrichMyCare.DTO.SourcePanel.Parameter.Program.Read
{
  public class FindProgramParameter
  {
    public int PatientNumber { get; set; }
  }

  public class FindProgramValidator : AbstractValidator<FindProgramParameter>
  {
    public FindProgramValidator()
    {
      RuleFor(x => x).NotNull();
      RuleFor(x => x.PatientNumber).GreaterThan(0).NotNull().NotEmpty();
    }
  }
}