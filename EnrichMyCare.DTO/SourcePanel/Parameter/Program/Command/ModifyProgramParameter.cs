﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.ParameterBuilder;
using EnrichMyCare.Entity.Program.ParameterBuilder;
using FluentValidation;
using System.Collections.Generic;

namespace EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command
{
  public class ModifyProgramParameter
  {
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public ExerciseProgramParameter ExerciseProgram { get; set; }
    public List<ExerciseParameter> ExerciseList { get; set; }

  }

  public class ModifyProgramValidator : AbstractValidator<ModifyProgramParameter>
  {
    public ModifyProgramValidator()
    {
      RuleFor(x => x).NotNull();
      RuleFor(x => x.ExerciseProgram).NotNull().NotEmpty();
      RuleFor(x => x.ExerciseList).NotNull().NotEmpty();
      RuleFor(x => x.PatientNumber).GreaterThan(0).NotNull().NotEmpty();
      RuleFor(x => x.ExerciseProgram.NameExercise).Length(1, 200).NotNull().NotEmpty();
      RuleFor(x => x.ExerciseProgram.ExerciseCreatedDate).NotNull().NotEmpty();
      RuleFor(x => x.ExerciseProgram.Goals).Length(1, 2000).NotNull().NotEmpty();

      RuleForEach(x => x.ExerciseList).ChildRules(exercise =>
      {
        exercise.RuleFor(x => x.ExerciseName).Length(1, 1000).NotNull().NotEmpty();
        exercise.RuleFor(x => x.ReviewDate).NotNull().NotEmpty();
        exercise.RuleFor(x => x.ExerciseActivity).Length(1, 7000).NotNull().NotEmpty();
        exercise.RuleForEach(y => y.RepetitionList).ChildRules(repetition =>
        {
          repetition.RuleFor(x => x.TimePeriod).GreaterThan(0).NotEmpty().NotNull();
          repetition.RuleFor(x => x.PerWeek).GreaterThan(0).NotEmpty().NotNull();
          repetition.RuleFor(x => x.Reps).GreaterThan(0).NotEmpty().NotNull();
        });
        exercise.RuleForEach(x => x.ExerciseProgramImagesList).ChildRules(exerciseProgramImage =>
        {
          exerciseProgramImage.RuleFor(x => x.FileName).Length(1, 200).Empty().NotNull();
        });
      });


    }
  }
}