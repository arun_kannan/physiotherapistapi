﻿using FluentValidation;

namespace EnrichMyCare.DTO.SourcePanel.Parameter.User.Command
{
  public class CreatePatientParameter
  {
    public string PatientName { get; set; }
    public string Diagnosis { get; set; }
  }

  public class CreatePatientParameterValidator : AbstractValidator<CreatePatientParameter>
  {
    public CreatePatientParameterValidator()
    {
      RuleFor(x => x).NotNull();
      RuleFor(x => x.PatientName).NotNull().NotEmpty();
      RuleFor(x => x.Diagnosis).NotNull().NotEmpty();
    }
  }
}