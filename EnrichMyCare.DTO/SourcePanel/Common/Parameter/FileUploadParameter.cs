﻿using FluentValidation;

namespace EnrichMyCare.DTO.SourcePanel.Common.Parameter
{
  public class FileUploadParameter
  {
    public string FileName { get; set; }
    public string FolderName { get; set; }
    public string FileString { get; set; }
  }

  public class FileUploadValidator : AbstractValidator<FileUploadParameter>
  {
    public FileUploadValidator()
    {
      RuleFor(x => x).NotNull();
      RuleFor(x => x.FileName).NotNull().NotEmpty().Length(4, 80);
      RuleFor(x => x.FolderName).NotNull().NotEmpty().Length(1, 300);
      RuleFor(x => x.FileString).NotNull().NotEmpty();
    }
  }
}