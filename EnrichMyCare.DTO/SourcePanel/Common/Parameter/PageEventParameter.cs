﻿namespace EnrichMyCare.DTO.SourcePanel.Common.Parameter
{
  public class PageEventParameter
  {
    public int PageIndex { get; set; }

    public int? PreviousPageIndex { get; set; }

    public int PageSize { get; set; }

    public int Length { get; set; }
  }
}