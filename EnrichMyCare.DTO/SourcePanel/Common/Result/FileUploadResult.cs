﻿namespace EnrichMyCare.DTO.SourcePanel.Common.Result
{
  public class FileUploadResult
  {
    public ServiceStatus ServiceStatus { get; set; }
    public FileUpload FileDetail { get; set; }
  }

  public class FileUpload
  {
    public string FileName { get; set; }
    public string FileUrl { get; set; }
  }
}