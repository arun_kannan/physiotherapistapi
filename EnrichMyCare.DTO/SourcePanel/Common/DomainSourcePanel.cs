﻿using EnrichMyCare.DTO.SourcePanel.Common.Parameter;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Read;
using EnrichMyCare.DTO.SourcePanel.Parameter.User.Command;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace EnrichMyCare.DTO.SourcePanel.Common
{
  public static class DomainSourcePanel
  {
    public static void Register(IServiceCollection services)
    {
      services.AddTransient<IValidator<FileUploadParameter>, FileUploadValidator>();
      services.AddTransient<IValidator<CreatePatientParameter>, CreatePatientParameterValidator>();
      services.AddTransient<IValidator<CreateProgramParameter>, CreateProgramValidator>();
      services.AddTransient<IValidator<ModifyProgramParameter>, ModifyProgramValidator>();
      services.AddTransient<IValidator<FindProgramParameter>, FindProgramValidator>();
    }
  }
}