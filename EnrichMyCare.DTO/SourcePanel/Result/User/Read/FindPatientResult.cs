﻿using System.Collections.Generic;

namespace EnrichMyCare.DTO.SourcePanel.Result.User.Read
{
  public class FindPatientResult
  {
    public List<FindPatient> FindPatient { get; set; }
    public ServiceStatus ServiceStatus { get; set; }
  }

  public class FindPatient
  {
    public int PatientNumber { get; set; }
    public string PatientName { get; set; }
    public string Diagnosis { get; set; }
  }
}