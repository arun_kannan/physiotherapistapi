﻿namespace EnrichMyCare.DTO.SourcePanel.Result.User.Command
{
  public class CreatePatientResult
  {
    public ServiceStatus ServiceStatus { get; set; }
  }
}