﻿namespace EnrichMyCare.DTO.SourcePanel.Result.Program.Command
{
  public class CreateProgramResult
  {
    public ServiceStatus ServiceStatus { get; set; }
  }
}