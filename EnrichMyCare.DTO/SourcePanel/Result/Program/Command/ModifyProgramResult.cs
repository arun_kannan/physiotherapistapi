﻿namespace EnrichMyCare.DTO.SourcePanel.Result.Program.Command
{
  public class ModifyProgramResult
  {
    public ServiceStatus ServiceStatus { get; set; }
  }
}