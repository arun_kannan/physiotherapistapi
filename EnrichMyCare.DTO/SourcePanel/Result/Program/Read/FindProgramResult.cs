﻿using EnrichMyCare.DTO.SourcePanel.Parameter.Program.Command;
using System.Collections.Generic;
using EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder;

namespace EnrichMyCare.DTO.SourcePanel.Result.Program.Read
{
  public class FindProgramResult
  {
    public List<ExerciseProgramResult> ExerciseProgramList { get; set; }
    public ServiceStatus ServiceStatus { get; set; }
  }
}