﻿namespace EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder
{
  public class ExerciseProgramImagesResult
  {
    public int ExerciseProgramImageId { get; set; }
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public string FileName { get; set; }
    public string FileUrl { get; set; }
  }
}