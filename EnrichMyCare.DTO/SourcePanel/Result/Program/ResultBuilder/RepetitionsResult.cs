﻿namespace EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder
{
  public class RepetitionsResult
  {
    public int ExerciseId { get; set; }
    public int RepetitionId { get; set; }
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public int Reps { get; set; }
    public int PerWeek { get; set; }
    public int TimePeriod { get; set; }
  }
}