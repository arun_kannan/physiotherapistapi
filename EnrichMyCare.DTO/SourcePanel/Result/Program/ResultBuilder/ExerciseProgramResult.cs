﻿using System;
using System.Collections.Generic;

namespace EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder
{
  public class ExerciseProgramResult
  {
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public string NameExercise { get; set; }
    public DateTime ExerciseCreatedDate { get; set; }
    public string Goals { get; set; }
    public List<ExerciseResult> ExerciseResultList { get; set; }
    public List<ExerciseProgramImagesResult> ExerciseProgramImageList { get; set; }
  
  }
}