﻿using System;
using System.Collections.Generic;

namespace EnrichMyCare.DTO.SourcePanel.Result.Program.ResultBuilder
{
  public class ExerciseResult
  {
    public int ExerciseId { get; set; }
    public int ExerciseProgramId { get; set; }
    public int PatientNumber { get; set; }
    public string ExerciseName { get; set; }
    public DateTime ReviewDate { get; set; }
    public string ExerciseActivity { get; set; }
    public List<RepetitionsResult> RepetitionsList { get; set; }

  }
}
