﻿namespace EnrichMyCare.DTO
{
  public class ServiceStatus
  {
    public int Code { get; set; }
    public string Description { get; set; }
  }
}