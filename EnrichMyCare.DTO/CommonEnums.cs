﻿namespace EnrichMyCare.DTO
{
  public class CommonEnums
  {
    public enum DMLOperation
    {
      Create,
      Modify,
      Remove,
      RemoveAll,
      ChangePassword
    }

    public enum CommonServiceResponse
    {
      InternalServerException = 500
    }
  }
}