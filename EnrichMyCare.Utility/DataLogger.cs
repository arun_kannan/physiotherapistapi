﻿using EnrichMyCare.ApplicationSettings;
using System;
using System.IO;
using System.Text;

namespace EnrichMyCare.Utility
{
  public static class DataLogger
  {
    public static void ToMessageAndCompleteStacktrace(this Exception exception, AppIdentitySettings appIdentitySettings, string errorGuid)
    {
      if (appIdentitySettings.FilePathConfiguration.IsLog)
      {
        var e = exception;
        var builder = new StringBuilder();
        builder.AppendLine("Error Id: " + errorGuid);
        while (e != null)
        {
          builder.AppendLine("Exception type: " + e.GetType().FullName);
          builder.AppendLine("Message : " + e.Message);
          builder.AppendLine("Stacktrace:");
          builder.AppendLine(e.StackTrace);
          builder.AppendLine();
          e = e.InnerException;
        }

        WriteLogger(builder, "ErrorLog", appIdentitySettings.FilePathConfiguration.ErrorLogPath);
      }
    }

    public static string ToMessageAndCompleteStacktrace(this Exception exception)
    {
      var e = exception;
      var builder = new StringBuilder();
      while (e != null)
      {
        builder.AppendLine("Exception type: " + e.GetType().FullName);
        builder.AppendLine("Message : " + e.Message);
        builder.AppendLine("Stacktrace:");
        builder.AppendLine(e.StackTrace);
        builder.AppendLine();
        e = e.InnerException;
      }
      return builder.ToString();
    }

    public static void ProcessLog(this string customMessage, AppIdentitySettings appIdentitySettings)
    {
      if (appIdentitySettings.FilePathConfiguration.IsProcess)
      {
        var builder = new StringBuilder();
        builder.AppendLine(customMessage);
        builder.AppendLine();
        WriteLogger(builder, "Process", appIdentitySettings.FilePathConfiguration.ProcessLogPath);
      }
    }

    public static void TraceLog(this string log, AppIdentitySettings appIdentitySettings)
    {
      if (appIdentitySettings.FilePathConfiguration.IsShowTracer)
      {
        var builder = new StringBuilder();
        builder.AppendLine(log);
        builder.AppendLine();
        WriteLogger(builder, "WSLog", appIdentitySettings.FilePathConfiguration.ServiceLogPath);
      }
    }

    private static void WriteLogger(StringBuilder builder, string loggerType, string logPath)
    {
      try
      {
        //  string filepath = Convert.ToString(ConfigurationManager.AppSettings["LogPath"]);
        var filepath = Convert.ToString(logPath);
        if (!Directory.Exists(filepath))
          Directory.CreateDirectory(filepath);

        switch (loggerType)
        {
          case "ErrorLog":
            filepath = string.Concat(filepath, "Error", DateTime.Today.ToString("dd-MM-yy") + ".log");
            break;

          case "WSLog":
            filepath = string.Concat(filepath, "WSLog", DateTime.Today.ToString("dd-MM-yy") + ".log");
            break;

          default:
            string.Concat(filepath, "NLog", DateTime.Today.ToString("dd-MM-yy") + ".log"); //Text File Name
            break;
        }

        if (!File.Exists(filepath))
          File.Create(filepath).Dispose();

        using (var sw = File.AppendText(filepath))
        {
          var line = Environment.NewLine + Environment.NewLine;
          if (loggerType == "ErrorLog")
          {
            sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now + "-----------------");
            sw.WriteLine("-------------------------------------------------------------------------------------");
            sw.WriteLine(line);
            sw.WriteLine(builder);
            sw.WriteLine("--------------------------------*End*------------------------------------------");
            sw.WriteLine(line);
          }
          else
          {
            sw.WriteLine(line);
            sw.WriteLine(builder);
          }

          sw.Flush();
          sw.Close();
        }
      }
      catch
      {
      }
    }
  }
}