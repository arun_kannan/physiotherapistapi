﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace EnrichMyCare.Utility
{
  public static class ImagesExtension
  {
    public static byte[] ToByteArray(this string base64String)
    {
      byte[] stringBytes = Convert.FromBase64String(base64String);
      return stringBytes;
    }

    public static string To64BaseString(this byte[] byteArray)
    {
      string base64String = Convert.ToBase64String(byteArray);
      return base64String;
    }

    public static string ImageFormatGet(ImageFormat rawFormat)
    {
      string format = string.Empty;
      try
      {
        if (rawFormat.Equals((ImageFormat.Jpeg)))
          format = ImageFormat.Jpeg.ToString();
        else if (rawFormat.Equals(ImageFormat.Bmp))
          format = ImageFormat.Bmp.ToString();
        else if (rawFormat.Equals(ImageFormat.Emf))
          format = ImageFormat.Emf.ToString();
        else if (rawFormat.Equals(ImageFormat.Exif))
          format = ImageFormat.Exif.ToString();
        else if (rawFormat.Equals(ImageFormat.Gif))
          format = ImageFormat.Gif.ToString();
        else if (rawFormat.Equals(ImageFormat.Icon))
          format = ImageFormat.Icon.ToString();
        else if (rawFormat.Equals(ImageFormat.Png))
          format = ImageFormat.Png.ToString();
        else if (rawFormat.Equals(ImageFormat.Tiff))
          format = ImageFormat.Tiff.ToString();
        else if (rawFormat.Equals(ImageFormat.Wmf))
          format = ImageFormat.Wmf.ToString();
      }
      catch (Exception)
      {
        throw;
      }
      return format;
    }

    public static Image Base64ToImage(string base64String)
    {
      byte[] imageBytes = Convert.FromBase64String(base64String);
      MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
      ms.Write(imageBytes, 0, imageBytes.Length);
      Image img = Image.FromStream(ms, true);
      return img;
    }

    public static string ImageToBase64(string filePath)
    {
      string base64String = null;
      using (var image = Image.FromFile(filePath))
      {
        using (var ms = new MemoryStream())
        {
          image.Save(ms, image.RawFormat);
          byte[] imageBytes = ms.ToArray();
          base64String = Convert.ToBase64String(imageBytes);
        }
      }
      return base64String;
    }

    public static string ImageToBase64(Image img, System.Drawing.Imaging.ImageFormat format, string filePath = null)
    {
      string base64String = null;
      try
      {
        if (string.IsNullOrEmpty(filePath))
        {
          using (var ms = new MemoryStream())
          {
            img.Save(ms, ImageFormat.Png);
            Byte[] imageBytes = ms.ToArray();
            base64String = Convert.ToBase64String(imageBytes);
          }
        }
        else if (!string.IsNullOrEmpty(filePath))
        {
          using (var image = Image.FromFile(filePath))
          {
            using (var ms = new MemoryStream())
            {
              image.Save(ms, image.RawFormat);
              byte[] imageBytes = ms.ToArray();
              base64String = Convert.ToBase64String(imageBytes);
            }
          }
        }
      }
      catch (Exception)
      {
        throw;
      }

      return base64String;
    }

    public static Bitmap CreateThumbnail(Image image, int lnWidth, int lnHeight)
    {
      System.Drawing.Bitmap bmpOut = null;
      try
      {
        Bitmap loBmp = new Bitmap(image);
        ImageFormat loFormat = loBmp.RawFormat;

        decimal lnRatio;
        int lnNewWidth = 0;
        int lnNewHeight = 0;

        //*** If the image is smaller than a thumbnail just return it
        if (loBmp.Width < lnWidth && loBmp.Height < lnHeight)
          return loBmp;

        if (loBmp.Width > loBmp.Height)
        {
          lnRatio = (decimal)lnWidth / loBmp.Width;
          lnNewWidth = lnWidth;
          decimal lnTemp = loBmp.Height * lnRatio;
          lnNewHeight = (int)lnTemp;
        }
        else
        {
          lnRatio = (decimal)lnHeight / loBmp.Height;
          lnNewHeight = lnHeight;
          decimal lnTemp = loBmp.Width * lnRatio;
          lnNewWidth = (int)lnTemp;
        }
        bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
        Graphics g = Graphics.FromImage(bmpOut);
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low;
        g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
        g.DrawImage(loBmp, 0, 0, lnNewWidth, lnNewHeight);

        loBmp.Dispose();
      }
      catch (Exception)
      {
        throw;
      }
      return bmpOut;
    }

    public static void CreateThumbnail(double scaleFactor, Image sourceImage, string targetPath, string fileName)
    {
      using (var image = sourceImage)
      {
        var newWidth = (int)(image.Width * scaleFactor);
        var newHeight = (int)(image.Height * scaleFactor);
        var thumbnailImg = new Bitmap(newWidth, newHeight);
        var thumbGraph = Graphics.FromImage(thumbnailImg);
        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
        thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
        var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
        thumbGraph.DrawImage(image, imageRectangle);
        SaveImage(thumbnailImg, fileName, targetPath);
        /*thumbnailImg.Save(targetPath, image.RawFormat)*/
        ;
      }
    }

    public static void SaveImage(Image image, string filename, string filePath)
    {
      try
      {
        CreateDirectory(filePath);
        var imagePath = Path.Combine(filePath, filename);
        RemoveImageInPath(imagePath);
        image.Save(imagePath);
      }
      catch (Exception)
      {
        throw;
      }
    }

    public static void CreateDirectory(string filePath)
    {
      bool exists = System.IO.Directory.Exists(filePath);

      if (!exists)
        System.IO.Directory.CreateDirectory(filePath);
    }

    private static void RemoveImageInPath(string imagePath)
    {
      if (System.IO.File.Exists(imagePath))
      {
        System.IO.File.Delete(imagePath);
      }
    }
  }
}